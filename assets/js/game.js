let cells ;

const PLAYER = ["O", "X"]

//va nous permettre d'alterner les joueurs
let wichPlayer = 0 ;

//cells des combinaisons gagnantes
const COMBINATION = [
	[0,1,2],
	[3,4,5],
	[6,7,8],
	[0,3,6],
	[1,4,7],
	[2,5,8],
	[0,4,8],
	[2,4,6],
]

const CELL_SELECT = document.querySelectorAll(".cells") ;

//quelle type de jeu (contre player ou contre IA)
let setGame = 0 ;
let setIA = 0 ;

//on assigne des nombres à des variables en fonction du click pour determiner quel type de jeu va etre jouer puis on lance la fonction game()
document.getElementById("contreIA").addEventListener("click", ()=>{
	setGame = 1 ;
	document.querySelector(".choix").style.display = "none" ;
	document.querySelector(".niveau").style.display = "block" ;
	document.getElementById("facile").addEventListener("click", ()=>{
		setIA = 1 ;
		document.querySelector(".niveau").style.display = "none" ;
		game() ;
	});
	document.getElementById("normal").addEventListener("click", ()=>{
		setIA = 2 ;
		document.querySelector(".niveau").style.display = "none" ;
		game() ;
	});
	document.getElementById("difficile").addEventListener("click", ()=>{
		setIA = 3 ;
		document.querySelector(".niveau").style.display = "none" ;
		game() ;
	});
}) ;

document.getElementById("contreJoueur").addEventListener("click", ()=>{
	setGame = 2 ;
	document.querySelector(".choix").style.display = "none"
	game() ;
}) ;


//fonction qui alterne les joueurs en alternant la variable wichPlayer entre 0 et 1 (pour l'index du cells PLAYER)
function switchPlayer(){
	if (wichPlayer == 0) {
		wichPlayer = 1 ;
	}
	else wichPlayer = 0 ;
}


function game(){
	document.querySelector(".fin").style.display = "none" ;
	document.querySelector(".titre").style.display = "none" ;
	document.querySelector(".tictac").style.display = "block" ;
    document.getElementById("replay").style.display = "none" ;
	//on assigne un objet cells de 9 index (de 0 à 8) à la variable tableau
	cells = Array.from(Array(9).keys());
	//pour chaque case du CELL_SELECT(class des elements html)
	for (let i = 0 ; i < CELL_SELECT.length ; i++) {

		//on rend cliquable chaque case du cells, whoWin execute la fonction clickOn au click
		CELL_SELECT[i].addEventListener("click", clickOn, false) ;
	}
}

function clickOn(square){

	// on verifie qu'il reste des identifiants nombres dans le cells (si oui il reste des cases jouables)
	if (typeof cells[square.target.id] == "number") {

		//on lance la fonction turn whoWin permet au player actuel de jouer
		turn(square.target.id, PLAYER[wichPlayer]);

		//fonction si Joueur contre Joueur qui va alterner l'index du cells PLAYER
		if (setGame == 2) {
			switchPlayer() ;
		}

		//fonction si Joueur contre IA
		else if (setGame == 1) {
			setTimeout (()=>{
				if (!checkWinner(cells, PLAYER[0]) && !equality()) turn(bestShot(), PLAYER[1])}, 200)
		};
	}

	//si pas de winner retourne l'égalité
	return equality() ;
}

function turn(squareId, player){

	//on insert les clicks dans le cells avex le X ou le O en fonction du player
	cells[squareId] = player ;
	document.getElementById(squareId).innerText = player ;

	//on verifie si winner et on le stocke dans la variable winner, si il y a un gagnant on l'envoit en parametre à la fonction gameOver
	let winner = checkWinner(cells, player) ;
	if (winner) gameOver(winner) ;
}

//fonction qui verifie si il y a un winner
function checkWinner(cells, player) {

	//on concatene chaque click des joueurs de cells dans un tableau (a, []) vide dans la variable player
	let play = cells.reduce((a, e, i) =>
		(e === player) ? a.concat(i) : a, [])
	let winner = null ;

	//comparaison des index du cells avec celle du tableau de combinaisons gagnantes
	//on nomme "win" l'index de chaque cells du tableau COMBINATION et on les parcours dans une boucle
	for (let [index, win] of COMBINATION.entries()){

		// si en parcourant les elements de chaque cells "win" il y a un tableau qui correspond aux elements du tableau player précédent
		if (win.every(elem => play.indexOf(elem) > -1)) {

			//on assigne à la variable winner quel player win avec quelle combinaison
			winner = {index: index, player: player};
			break;
		}
	}
	return winner ;
}

function gameOver(winner) {

	//on parcours les tableaux de COMBINAISONS pour trouver le cells correspondant à celui du winner
	for (let index of COMBINATION[winner.index]) {

		//renvoit une couleur en fonction du winner sur la ligne gagnante
		document.getElementById(index).style.backgroundColor =
			winner.player == PLAYER[0,1] ? PLAYER[0] = "orange" : PLAYER[1] = "purple" ;
	}

	//stop l'ecouteur d'evenement sur toutes les cases après declaration du winner
	for (let i = 0 ; i < CELL_SELECT.length ; i++) {
		CELL_SELECT[i].removeEventListener("click", clickOn, false)
	}

	//on evite si il y a un winner au bout de 9 cases remplit que la fonction equality nous renvoit l'egalité
	equality = false ;

	//renvoit un message en fonction de l'index player du cells winner
	declareWinner(winner.player == PLAYER[0,1] ? PLAYER[0] = "VICTORY PLAYER 2 !" : PLAYER[1] = "VICTORY PLAYER 1 !")
}

function declareWinner(whoWin) {
	document.querySelector(".fin").style.display = "block" ;
	document.querySelector(".text").innerText = whoWin ;
    document.getElementById("replay").style.display = "inline" ;
}

//pour chaque carré vide on attend un nombre au cells sinon les cases ne sont pas vide
function EmptyCell() {
	return cells.filter(s => typeof s === "number");
}

function equality() {
	//on verifie l'égalité si il reste un carré vide du cells de 10 carré
	if (EmptyCell().length === 0) {
		for (let i = 0 ; i < CELL_SELECT.length ; i++) {
			CELL_SELECT[i].removeEventListener("click", clickOn, false) ;
		}
		declareWinner("EQUALITY !")
		return true ;
	}
	return false ;
}

//fonction whoWin renvoit l'algorithme minimax basé sur l'index des objets cells et player(IA)
function bestShot() {
	return minimax(cells, PLAYER[1]).index;
}

//fonction basé sur l'algo minimax qui va parcourir tout les choix de shots pour calculer le meilleur possible
function minimax(newCell, player) {

	//assignation des carres vides à une variable (va suivre le remplissage au fur et à mesure)
	let availableCell = EmptyCell();

	//verifie si il y a un winner et retourne un objet avec index score et une valeur en fonction de qui gagne, 0 si égalité

	//si winner player humain score negatif
	if (checkWinner(newCell, PLAYER[0])) {
		if (setIA == 1) return {score: Math.round(Math.random()*-3)};
		else if (setIA == 2) return {score: Math.round(Math.random()*-5)};
		else if (setIA == 3) return {score: -10};
	}

	//si winner IA score positif
	else if (checkWinner(newCell, PLAYER[1])) {
		if (setIA == 1) return {score: Math.round(Math.random()*3)};
		else if (setIA == 2) return {score: Math.round(Math.random()*10)};
		else if (setIA == 3) return {score: 20};
	}

	else if (availableCell.length === 1) {
		if (setIA == 1) return {score: Math.ceil(Math.random()*5)};
		if (setIA == 2) return {score: Math.ceil(Math.random()*10)};
		if (setIA == 3) return {score: 5};
	}

	//creation d'un cells whoWin va parcourir les carrés encore dispo
	let shots = [];
	for (let i = 0; i < availableCell.length; i++) {

		//chaque carré dispo va etre stocké dans un objet
		let move = {};

		//on assigne à move l'index des nombres stocké du cells des carrés encore dispo dans un nouveau tableau
		move.index = newCell[availableCell[i]];

		//assigne le nouveau cells au player actuel
		newCell[availableCell[i]] = player;

		//on stocke dans un objet chaque move joué par chaque player avec la fonction minimax
		if (player == PLAYER[1]) {
			let result = minimax(newCell, PLAYER[0]);

			//on stocke le score dans l'objet move avec la propriété score des objets
			move.score = result.score;
		}
		else {
			let result = minimax(newCell, PLAYER[1]);
			move.score = result.score;
		}

		//on reinitialise le cells des shots
		newCell[availableCell[i]] = move.index;

		//on push l'objet move dans le cells shots
		shots.push(move);
	}

	//on va calculer le meilleur move possible en fonction du score renvoyer
	let bestMove;
	if(player === PLAYER[1]) {

		//on assigne un score très bas à la variable bestScore
		let bestScore = -100000;

		//on parcourt le cells précédent whoWin stocké un score à chaque move pour chaque carré dispo
		for(let i = 0; i < shots.length; i++) {
			if (shots[i].score > bestScore) {
				//si un bon score trouvé on assigne ce score à la variable bestScore
				bestScore = shots[i].score;
				bestMove = i;
			}
			/*else if (shots[i].score = bestScore) {
				bestScore = shots[i].score ;
				coupMeilleur = i ;
			}*/
		}
	}

	//on fait la même chose mais en inversant pour le turn du player humain
	else {
		let bestScore = 100000;
		for(let i = 0; i < shots.length; i++) {
			if (shots[i].score < bestScore) {
				bestScore = shots[i].score;
				bestMove = i;
			}
			else if (shots[i].score >= bestScore) {
				bestScore = shots[i].score ;
				coupMeilleur = i ;
			}
		}
	}

	//on récupére le meilleur move à jouer pour l'IA
	return shots[bestMove];
}

